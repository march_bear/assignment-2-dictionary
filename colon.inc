%ifndef COLON_INC
%define COLON_INC

%xdefine ptr_to_next_elem 0

%macro colon 2
    %2:
    dq ptr_to_next_elem
    db %1, 0
    %xdefine ptr_to_next_elem %2
%endmacro
%endif
