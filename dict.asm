%define ADDRESS_LEN 8

section .text

%include "lib.inc"

global find_word

; rdi - адрес начала строки
; rsi - адрес начала списка
find_word:
    push r12
    push r13
    push r14
    mov r12, rdi
    mov r13, rsi
    .loop:
        test r13, r13
        jz .error
        mov r14, qword[r13]
        add r13, ADDRESS_LEN
        mov rdi, r12
        mov rsi, r13
        call string_equals

        test rax, rax
        jnz .success
        mov r13, r14
        jmp .loop
    .success:
        mov rdi, r13
        call string_length
        add rax, r13
        inc rax
        jmp .restore_regs
    .error:
        xor rax, rax
    .restore_regs:              ; восстановление значений используемых callee-saved регистров и выход из функции
        pop r14
        pop r13
        pop r12
        ret
        