%ifndef WORDS_INC
%define WORDS_INC
%define DICTIONARY_START first
%include "colon.inc"

section .data
colon "third word", third
db "it's third word", 0

colon "second word", second
db "it's second word", 0

colon "first word", DICTIONARY_START
db "it's first word", 0
%endif
